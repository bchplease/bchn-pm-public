## Flipstart Bitcoin Cash Node

Bitcoin Cash Node (BCHN) is a full node project which aims to
provide a safe client implementation for the BCH network, supported
by an engaged community of professional developers, testers and
supporting staff.

Our software already functions as a drop-in replacement for Bitcoin
ABC, following the longest chain for the May 2020 network upgrade
without contributing to the risk of chain split.

Our vision for BCH is for a network that is strong through both
competition and cooperation, running on a diverse array of client
software, living up to professional levels of development, maintenance
and support. We will help create an environment where decisions are backed by
research, evidence, and decision-making involves stakeholders and expertise
from across the ecosystem.

The Bitcoin Cash Node project has an open-door policy, welcoming all
levels of expertise and all corners of the Bitcoin Cash. We discuss matters in
the open, invite interested parties to participate, and hold ourselves to high
standards of accountability.

In this funding proposal, we hope to get the necessary resources to attend to
some immediate needs of Bitcoin Cash miners, businesses and users, providing
robust research and implementation for them. We also intend to put in place
some necessary structure to ensure we are equipped to sustain ourselves through
the next leg of challenges.

