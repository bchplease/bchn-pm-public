freetrader
(pseudonym)
BCHN lead maintainer

freetrader has worked professionally in the development and
maintenance of software systems both small and large, including
safety critical distributed systems. With experience spanning nearly
three decades and companies ranging from startups to medium and
large enterprises, he has knowledge of the full software project
life cycle and held roles in technical support, system
administration, software engineering, project and quality management.

Freetrader shares a vision of Bitcoin as censorship-resistant, truly
decentralized peer-to-peer electronic cash for the world. His
investments in Bitcoin and observations of the scaling debates of
2013-2016 motivated his efforts to acquaint himself with the
software. In 2016 he worked on the BTCfork project which created
minimum viable hard-fork clients based on Bitcoin Core and Bitcoin
Unlimited. In 2017 he co-developed Bitcoin Cash, working primarily
as an ABC developer but collaborating with several client projects
(BU, Classic, XT) to establish the common specification for Bitcoin
Cash. Since late 2017 he focused his effort on promotion of Bitcoin
Cash.

He leads the BCHN project to avoid a damaging split of the BCH
currency and continue the scaling roadmap. Long term he hopes that
it can help to fill a market need for a high-quality, reliable open
source node project that is welcoming to all, operates sustainably
and with good governance according to transparent, accountable
processes.
